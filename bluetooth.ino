// Portbelegung
// 2      IN4     rot
// 3     INB     braun
// 4     IN3     orange
// 5     IN2     gelb
// 6     INA     blau
// 7     IN1     grün


void setup() {
  Serial.begin(9600);  //Set the baud rate to your Bluetooth module.

  setup_car();
}


void loop() {
  char command = Serial.read();

  drive_stop();

  switch (command) {
    case 'F': // Forward
      drive_forward(255);
      delay(10);
      break;
    case 'B': // Backward
      drive_backward(255);
      delay(10);
      break;
    case 'L': // Left
      turn_left(10, 255);
      break;
    case 'R': // Right
      turn_right(10, 255);
      break;
    case 'G': // Forward Left
      drive_forward(127, 255);
      delay(10);
      break;
    case 'I': // Forward Right
      drive_forward(255, 127);
      delay(10);
      break;
    case 'H': // Backward Left
      drive_backward(127, 155);
      delay(10);
      break;
    case 'J': // Backward Right
      drive_backward(255, 127);
      delay(10);
      break;
  }
}
