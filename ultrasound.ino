void setup() {
  // The Serial Monitor is mostly used for debugging
  Serial.begin(9600);
  setup_car();
  set_servo_angle(40);
}


void loop() {
  // Tries to avoid hitting a wall by turning right if the distance is to small
  // Serial.println( get_distance() );
  Serial.println(get_servo_angle());

  if (get_distance() > 60) {
    drive_forward(100);
    delay(200);
  }
  else {
    turn_right(500, 100);
    delay(200);
  }

  if (get_servo_angle() > 140) {
    set_servo_angle(40);
  }
  else {
    turn_servo_left(15);
  }
}