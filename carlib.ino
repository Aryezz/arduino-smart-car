/**
   This is the main library to control our Arduino car
   The following features are implemented and tested:
    - Driving Forwards and Backwards
    - Turning the car Right and Left
    - Reading the Distance of the ultrasound sensor in centimeters
    - Turning the servo Right and Left and setting it to a certain angle
    - Reading the current angle of the servo

    @author Samuel Burkhardt
    @author Noah Grossenbacher
    @version 1.0
*/
#include <Servo.h>


const int motor_left_f = 7;  // PIN 14 of L293
const int motor_left_b = 5;  // PIN 10 of L293
const int motor_left_s = 6;  // left motor speed

const int motor_right_f = 2; // PIN 7 of L293
const int motor_right_b = 4; // PIN 2 of L293
const int motor_right_s = 3; // right motor speed

const int ultra_trigger = 8; // Trigger for ultrasound
const int ultra_echo = 9;    // Echo for ultrasound

Servo car_servo; // Servo Object to control the servo motor that spins the ultrasound ranging module


long get_distance() {
  /**
     Returns the distance of the ultrasound in cm

     Returns:
     --------
     distance : long
         The distance the ultrasound sensor provides in cm

  */
  long pulse = 0;
  long distance = 0;

  digitalWrite(ultra_trigger, LOW);
  delay(5);
  digitalWrite(ultra_trigger, HIGH);
  delay(10);
  digitalWrite(ultra_trigger, LOW);

  pulse = pulseIn(ultra_echo, HIGH);

  // This formula was provided in our documents
  distance = (pulse / 2) * 0.03432;

  return distance;
}


void drive_forward(int speed_) {
  /**
     Starts both motors to drive forwards

     Parameters:
     -----------
     speed : int
         The speed at which to drive forwards
  */
  analogWrite(motor_left_s, speed_-5);
  analogWrite(motor_right_s, speed_);

  digitalWrite(motor_left_b, LOW);
  digitalWrite(motor_left_f, HIGH);
  digitalWrite(motor_right_b, LOW);
  digitalWrite(motor_right_f, HIGH);
}


void drive_forward(int speed_left, int speed_right) {
  /**
     Starts both motors to drive forwards

     Parameters:
     -----------
     speed_left : int
         The speed at which to drive forwards with the left motor
     speed_right : int
         The speed at which to drive forwards with the right motor
  */
  analogWrite(motor_left_s, speed_left-5);
  analogWrite(motor_right_s, speed_right);
  
  digitalWrite(motor_left_f, HIGH);
  digitalWrite(motor_left_b, LOW);
  digitalWrite(motor_right_f, HIGH);
  digitalWrite(motor_right_b, LOW);
}


void drive_backward(int speed_) {
  /**
     Starts both motors to drive backwards

     Parameters:
     -----------
     speed : int
         The speed at which to drive backwards
  */
  analogWrite(motor_left_s, speed_-5);
  analogWrite(motor_right_s, speed_);

  digitalWrite(motor_left_f, LOW);
  digitalWrite(motor_left_b, HIGH);
  digitalWrite(motor_right_f, LOW);
  digitalWrite(motor_right_b, HIGH);
}


void drive_backward(int speed_left, int speed_right) {
  /**
     Starts both motors to drive backwards

     Parameters:
     -----------
     speed_left : int
         The speed at which to drive backwards with the left motor
     speed_right : int
         The speed at which to drive backwards with the right motor
  */
  analogWrite(motor_left_s, speed_left-5);
  analogWrite(motor_right_s, speed_right);
  
  digitalWrite(motor_left_f, LOW);
  digitalWrite(motor_left_b, HIGH);
  digitalWrite(motor_right_f, LOW);
  digitalWrite(motor_right_b, HIGH);
}


void drive_stop() {
  /**
     Stops both motors and sets the speed to 0
  */
  analogWrite(motor_left_s, 0);
  analogWrite(motor_right_s, 0);

  digitalWrite(motor_left_f, LOW);
  digitalWrite(motor_left_b, LOW);
  digitalWrite(motor_right_f, LOW);
  digitalWrite(motor_right_b, LOW);
}


void turn_left(int duration, int speed_) {
  /**
     Turns the car to the left by driving forwards on the right and backwards on the left motor

     Parameters:
     -----------
     duration : int
         The duration to keep turning left
         The higher this value is, the further left it turns
     speed : int
         The speed at which to turn
         Defaults to 60
  */
  analogWrite(motor_left_s, speed_-5);
  analogWrite(motor_right_s, speed_);

  digitalWrite(motor_left_f, LOW);
  digitalWrite(motor_left_b, HIGH);
  digitalWrite(motor_right_f, HIGH);
  digitalWrite(motor_right_b, LOW);
  delay(duration);
  drive_stop();
}


void turn_right(int duration, int speed_) {
  /**
     Turns the car to the right by driving forwards on the left and backwards on the right motor

     Parameters:
     -----------
     duration : int
         The duration to keep turning right
         The higher this value is, the further right it turns
     speed : int
         The speed at which to turn
         Defaults to 60
  */
  analogWrite(motor_left_s, speed_-5);
  analogWrite(motor_right_s, speed_);

  digitalWrite(motor_left_f, HIGH);
  digitalWrite(motor_left_b, LOW);
  digitalWrite(motor_right_f, LOW);
  digitalWrite(motor_right_b, HIGH);
  delay(duration);
  drive_stop();
}


int get_servo_angle() {
  /**
     Returns the current angle of the servo motor
  */
  return car_servo.read();
}


void set_servo_angle(int angle) {
  /**
     Sets the servo to a given angle

     Parameters:
     -----------
     angle : int
         the new angle to rotate the servo to
  */
  if (angle < 0) {
    angle = 0;
  }
  else if (angle > 180) {
    angle = 180;
  }
  
  car_servo.write(angle);
}


void turn_servo_right(int rotation) {
  /**
     Turns the servo right by a given angle

     Parameters:
     -----------
     rotation : int
         The amount of degrees to turn to the right
  */
  int angle = get_servo_angle() - rotation;
  
  car_servo.write(angle);
}


void turn_servo_left(int rotation) {
  /**
     Turns the servo left by a given angle

     Parameters:
     -----------
     rotation : int
         The amount of degrees to turn to the left
  */
  int angle = get_servo_angle() + rotation;

  car_servo.write(angle);
}


void setup_car() {
  /**
     Sets up the car
  */
  
  pinMode(motor_left_f, OUTPUT);
  pinMode(motor_left_b, OUTPUT);
  pinMode(motor_left_s, OUTPUT);

  pinMode(motor_right_f, OUTPUT);
  pinMode(motor_right_b, OUTPUT);
  pinMode(motor_right_s, OUTPUT);

  pinMode(ultra_trigger, OUTPUT);

  car_servo.attach(10); // attach the signal pin of servo to pin9 of arduino
  car_servo.write(0); // Reset the Servo Rotation
}
